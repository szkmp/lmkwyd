# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lmkwyd/version'

Gem::Specification.new do |spec|
  spec.name          = "lmkwyd"
  spec.version       = Lmkwyd::VERSION
  spec.authors       = ["Tatsuya Suzuki"]
  spec.email         = ["sin.wave808@gmail.com"]

  spec.summary       = %q{Mac terminal command to notify when a specified command is done by displaying a banner of Notification Center}
  spec.description   = %q{Mac terminal command to notify when a specified command is done by displaying a banner of Notification Center}
  spec.homepage      = "https://gitlab.com/szkmp/lmkwyd"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
