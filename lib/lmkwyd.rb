require 'lmkwyd/version'
require 'lmkwyd/commander'

module Lmkwyd
  def self.exec
    commander = Commander.new(ARGV.join(' '))
    exit commander.exec.tap {
      Lmkwyd::Notifier.new(commander).notify
    }
  end
end
