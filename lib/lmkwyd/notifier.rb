class Lmkwyd::Notifier
  attr_reader :commander

  def initialize(commander)
    @commander = commander
  end

  def notify
    `#{command}`
  end

  def command
    %Q|osascript -e \
       'display notification "#{body}" \
        with title "Command completed" \
        subtitle "#{commander.command}" \
        sound name "Hero"'|
  end

  private

  def body
    # TODO make this Linux compatible
    "Time to execute: #{humanize_length_of_time(commander.duration)}"
  end

  def humanize_length_of_time(seconds)
    Time.at(seconds).utc.strftime("%H:%M:%S")
  end
end
