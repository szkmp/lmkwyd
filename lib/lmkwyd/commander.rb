require 'open3'
require 'lmkwyd/notifier'

# Executes user specified command line
#
# @example General usage
#   lmkwyd sleep 3
#   # => displays notification with title "Command Completed", subtitle "sleep 3" and body saying "Time to execute: 00:00:03"
#   # => ![](https://gitlab.com/szkmp/lmkwyd/uploads/65793e28551695b4fd16348db0bd41ea/Screen_Shot_2016-05-08_at_4.40.41_PM.png)
class Lmkwyd::Commander
  EXIT_CODE_OK = 0

  attr_reader :command, :duration

  def initialize(command_string)
    @command = command_string
  end

  # Executes user specified command line
  #
  # @return [integer] Exit code of user specified command line
  # @note This returns exit code 1 (General Error) if one of multiple commands in the command line fails
  # @example Execute multiple commands
  #   lmkwyd 'sleep 1 && echo Hey!'
  #   # => Sleeps 1 sec and displays notification saying "Hey!"
  #
  #   lmkwyd 'ls /inexistent && echo Hey!'
  #   # => Displays notification and prints stdout 'ls: /inexistent: No such file or directory'
  #   # => echo Hey! is not executed, as same result as `lmkwyd ls /inexistent && echo Hey!` (without single quot)
  #
  #   lmkwyd 'echo Hey! && sleep 10'
  #   # => Displays notification 10 seconds after both spcified commands are completed
  def exec
    exit_code = EXIT_CODE_OK

    # TODO Check system ruby version
    @duration = measure_time_consumption {
        exit_code = run_command
      }

    exit_code
  end

  private

  def run_command
    Open3.popen3(command) {|stdin, stdout, stderr, wait_thr|
      stdout.each {|line| print line }
      stderr.each {|line| print line }
      wait_thr.value.exitstatus
    }
  end

  def measure_time_consumption
    start = Time.now
    yield
    Time.now - start
  end
end
