require 'spec_helper'

describe Lmkwyd do
  it 'has a version number' do
    expect(Lmkwyd::VERSION).not_to be nil
  end

  context "when notifier returns exit code " do
    before do
      Lmkwyd::ARGV = ['echo', 'foo']
      allow(Lmkwyd::Notifier).to receive_message_chain(:new, :notify).and_return(1)
    end

    it 'exits with code 1' do
      expect{ Lmkwyd.exec }.to raise_error(SystemExit)
    end
  end
end
