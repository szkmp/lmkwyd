require 'rspec'
require 'rspec/expectations'
require 'lmkwyd/commander'

RSpec::Matchers.define :be_ok do |expected|
  match do |actual|
    actual == 0
  end
end
RSpec::Matchers.define :be_general_error do |expected|
  match do |actual|
    actual == 1
  end
end

describe Lmkwyd::Commander do
  describe "#exec" do
    let(:commander) do
      Lmkwyd::Commander.new(command)
    end

    context "with valid command" do
      let(:command) do
        'echo foo'
      end

      before do
        allow(commander).to receive(:measure_time_consumption).and_return(30)
        commander.exec
      end

      it "sets duration" do
        expect(commander.duration).to eq(30)
      end
    end

    context "when specified command is successful" do
      let(:command) { 'echo foo' }

      it "returns exit code of OK" do
        expect(commander.exec).to be_ok
      end
    end

    context "when specified command isn't successful" do
      let(:command) { 'exit 1' }

      it "returns exit code of general error" do
        expect(commander.exec).to be_general_error
      end

    end

    context "with multiple commands specified" do
      context "and one of the commands fails" do
        let(:command) do
          'exit 1 && echo foo'
        end

        it "returns exit code of general error" do
          expect(commander.exec).to be_general_error
        end
      end

      context "and all the commands are successful" do
        let(:command) do
          'echo foo && echo bar'
        end

        it "returns exit code of OK" do
          expect(commander.exec).to be_ok
        end
      end
    end

    context "when command with multiple lines specified" do
      let(:command) do
        'echo \
        foo'
      end

      it "returns exit code of OK" do
        expect(commander.exec).to be_ok
      end
    end
  end
end
