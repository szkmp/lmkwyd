require 'lmkwyd/notifier'
require 'lmkwyd/commander'

describe Lmkwyd::Notifier do
  describe "#command" do
    context "when command takes 5 minutes and 32 seconds" do
      let(:command_string) do
        "ls /a"
      end
      let(:commander) do
        Lmkwyd::Commander.new(command_string)
      end
      let(:notifier) do
        Lmkwyd::Notifier.new(commander)
      end

      before do
        allow(commander).to receive(:duration).and_return(5 * 60 + 32)
      end

      it "matches 05:32" do
        expect(notifier.command).to match('05:32')
      end
      it "matches specified command string" do
        expect(notifier.command).to match(command_string)
      end
    end
  end
end
