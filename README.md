# Lmkwyd

Mac terminal command to notify when a specified commands is done by displaying a banner of Notification Center

`lmkwyd` stands for `Let me know whe you're done`.

## Usage

### General Usage:

```shell
lmkwyd sleep 3
# => displays notification with title "Command Completed", subtitle "sleep 3" and body saying "Time to execute: 00:00:03"
```

![](https://gitlab.com/szkmp/lmkwyd/uploads/65793e28551695b4fd16348db0bd41ea/Screen_Shot_2016-05-08_at_4.40.41_PM.png)


lmkwyd doesn't change exit code of commands a user specified. When
consecutive commands are specified lmkwyd exits with exit code of their
final result.

### Usage (variation):

```shell
lmkwyd 'sleep 1 && echo Hey!'
# => Sleeps 1 sec and displays notification saying "Hey!"

lmkwyd 'ls /inexistent && echo Hey!'
# => Displays notification and prints stdout 'ls: /inexistent: No such file or directory'
# => echo Hey! is not executed, as same result as `lmkwyd ls /inexistent && echo Hey!` (without single quot)

lmkwyd 'echo Hey! && sleep 10'
# => Displays notification 10 seconds after both spcified commands are completed
```


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'lmkwyd'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install lmkwyd

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at `https://gitlab.com/szkmp/lmkwyd`.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Alterntaives
Alternatives with bash script [^](https://askubuntu.com/questions/409611/desktop-notification-when-long-running-commands-complete/409766)
